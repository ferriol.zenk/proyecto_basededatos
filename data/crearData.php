<html>
<meta charset="utf-8">
<head>
</head>
<body>
<?php
session_start();
$bd = $_SESSION["nick"];


require("conecxion.php");//uso del script con la conexion

$ok = true;
//BASE DE DATOS
$consulta="CREATE DATABASE if not exists $bd CHARACTER SET UTF8;";

$resultado = $mysqli->query($consulta);

if (!$resultado) 
  {echo "Lo sentimos. La Aplicación no funciona<br>";
   echo "Error. en la consulta: ".$consulta."<br>";
   echo "Num.error: ".$mysqli->errno."<br>";
   echo "Error: ".$mysqli->error. "<br>";
   $ok=false;
   exit;
  }
//Me posicion sobre la tabla
$nombreBase=$bd;
$mysqli->select_db($nombreBase); 
//TABLA PLATAFORMA
$consulta="CREATE TABLE if NOT EXISTS  plataforma(";
$consulta.="nomPlat VARCHAR(50) NOT NULL,";
$consulta.="dispositivo VARCHAR(50) NOT NULL,";
$consulta.="numJuegos INT NULL,";
$consulta.="PRIMARY KEY (nomPlat));";

$resultado = $mysqli->query($consulta);

if (!$resultado) 
  {echo "Lo sentimos. La Aplicación no funciona<br>";
   echo "Error. en la consulta: ".$consulta."<br>";
   echo "Num.error: ".$mysqli->errno."<br>";
   echo "Error: ".$mysqli->error. "<br>";
   $ok=false;
   exit;
  }

//TABLA VIDEOJUEGP
$consulta="CREATE TABLE if NOT EXISTS videojuego(";
$consulta.="id INT AUTO_INCREMENT,";
$consulta.="nomJuego VARCHAR(50) NOT NULL,";
$consulta.="nomPlat VARCHAR(50) NULL,";
$consulta.="genero VARCHAR(50) NOT NULL,";
$consulta.="numHoras INT NULL,";
$consulta.="valoracion INT(1) NULL,";
$consulta.="opinion VARCHAR(500) NULL,";
$consulta.="PRIMARY KEY (id),";
$consulta.="FOREIGN KEY (nomPlat) REFERENCES plataforma (nomPlat) ON DELETE SET NULL);";

$resultado = $mysqli->query($consulta);

if (!$resultado) 
  {echo "Lo sentimos. La Aplicación no funciona<br>";
   echo "Error. en la consulta: ".$consulta."<br>";
   echo "Num.error: ".$mysqli->errno."<br>";
   echo "Error: ".$mysqli->error. "<br>";
   $ok=false;
   exit;
  }

$consulta="CREATE TRIGGER `videojuego_before_insert` BEFORE INSERT ON `videojuego` FOR EACH ROW";
$consulta.=" UPDATE plataforma SET numJuegos=numJuegos+1 WHERE nomPlat=NEW.nomPlat;";
echo $consulta;

$resultado = $mysqli->query($consulta);

if (!$resultado) 
  {echo "Lo sentimos. La Aplicación no funciona<br>";
   echo "Error. en la consulta: ".$consulta."<br>";
   echo "Num.error: ".$mysqli->errno."<br>";
   echo "Error: ".$mysqli->error. "<br>";
   $ok=false;
   exit;
  }

$consulta="CREATE TRIGGER `videojuego_after_delete` AFTER DELETE ON `videojuego`FOR EACH ROW ";
$consulta.=" UPDATE plataforma SET numJuegos=numJuegos-1 WHERE nomPlat=OLD.nomPlat;";


$resultado = $mysqli->query($consulta);

if (!$resultado) 
  {echo "Lo sentimos. La Aplicación no funciona<br>";
   echo "Error. en la consulta: ".$consulta."<br>";
   echo "Num.error: ".$mysqli->errno."<br>";
   echo "Error: ".$mysqli->error. "<br>";
   $ok=false;
   exit;
  }
header("Location: ../infoJuegos.php")

?>
</body>