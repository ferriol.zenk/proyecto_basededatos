<html>
<head>
<link rel="stylesheet" type="text/css" href="css/cssform.css">
<meta charset="utf-8">
<script language="javascript" type="text/javascript">
    function saltar(pagina){
        document.form1.action=pagina;
        document.form1.submit();
    }
</script>
<?php 
  session_start();
  if (isset($_REQUEST["seleccionJuego"]))
  {
    require("data/conecxion.php");
    $seleccion=$_REQUEST["seleccionJuego"];
    $consulta="SELECT * FROM videojuego WHERE id='$seleccion';";
    $resultado=$mysqli->query($consulta);

    $datos = $resultado->fetch_assoc();
    $id=$datos["id"];
    $nomJuego=$datos["nomJuego"];
    $nomPlat=$datos["nomPlat"];
    $genero=$datos["genero"];
    $numHoras=$datos["numHoras"];
    $valoracion=$datos["valoracion"];
    $opinion=$datos["opinion"];
  }else{
    $id="";
    $nomJuego="";
    $nomPlat=""; 
    $genero="";
    $numHoras=0;
    $valoracion=5;
    $opinion="";
  }
?>
<script language="javascript" type="text/javascript">
  function mostrarDatos (){
    document.getElementById("id").value="<?php echo ($id);?>";
    document.getElementById("nomJuego").value="<?php echo ($nomJuego); ?>";
    document.getElementById("genero").value="<?php echo ($genero); ?>";
    document.getElementById("numHoras").value="<?php echo ($numHoras); ?>";
    document.getElementById("valoracion").value="<?php echo ($valoracion); ?>";
    document.getElementById("opinion").value="<?php echo ($opinion); ?>";
    document.getElementById("nomPlat").value="<?php echo ($nomPlat); ?>";
  }
    function LimpiarDatos (){
    document.getElementById("id").value="<?php echo ($id);?>";
    document.getElementById("nomJuego").value="";
    document.getElementById("genero").value="";
    document.getElementById("numHoras").value="";
    document.getElementById("valoracion").value="";
    document.getElementById("opinion").value="";
    document.getElementById("nomPlat").value="";
  }
</script>
</script>
</head>
<body onLoad="javascript:mostrarDatos();" style="background-image: url('imagenes/imagen1mod.jpg')">
<form id="form1" name="form1" method="post" action="" >
  <h2>FORMULARIO JUEGOS</h2>
  <input type="hidden" name="id" id="id" value="$id"/> <!--input oculto solo para mover el id-->
  <p>
    <label for="nomJuego">*Nombre Juego: </label>
    <input type="text" name="nomJuego" id="nomJuego" value="$nomJuego" />
  </p>
  <p>
    <label for="nomPlat">*Plataforma: </label>
    <select name="nomPlat" id="nomPlat">
      <?php 
      require ("data/conecxion.php");
      $consulta ="SELECT nomPlat FROM plataforma;";
      $resultado=$mysqli->query($consulta);
      if (isset($nomPlat)){echo ("<option selected>"."$nomPlat"."</option>");}
      while ($fila = $resultado ->fetch_assoc())
          {
            echo ("<option>".$fila["nomPlat"]."</option>");
          }  
      ?>
    </select>
  </p>
  <p>
    <label for="genero">*Genero: </label>
    <input type="text" name="genero" id="genero" value="$genero" />
  </p>
  <p>
    <label for="numHoras">Numero de Horas: </label>
    <input type="text" name="numHoras" id="numHoras"  value="$numHoras"/>
  </p>
  <p>
    <label for="val">Valoracion:  <input type="range" name="valoracion" id="valoracion"
    min="0" max="10" step="1" value="$valoracion" list="ticks"> </label>
    <datalist id="ticks">
      <option value="0" label="0">
      <option value="1">
      <option value="2">
      <option value="3">
      <option value="4">
      <option value="5" laber="5">
      <option value="6">
      <option value="7">
      <option value="8">
      <option value="9">
      <option value="10" label="10">
    </datalist>
  </p>
  <label for="op">Opinion: </label>
  <p>
    <?php
    echo("<textarea name='opinion' rows='4' cols='40' placeholder='escribe aquí'>$opinion</textarea>")
    ?>
  </p>
    <input id="button" type="submit" value="Enviar" onclick='javascript:saltar("procesamientoDatos/registroJuegos.php")'/>
    <input id="button" type="submit" value="Modificar" onclick='javascript:saltar("procesamientoDatos/modificarJuegos.php")' />
    <input id="button" type="submit" value="Volver" onclick='javascript:saltar("infoJuegos.php")'/>
    <input id="button" type="button" value="Limpiar" onclick='javascript:LimpiarDatos()'/>
    <p>*Campos obligatorios</p>
  </p>
  </form>
</body>
</html>
