
<!--Comprueba si la base de datos esta creada, si no la crea-->
<?php
session_start();
$bd = $_SESSION["nick"];
require ("data/conecxion.php");  
$nombreBase="libreria_juegos";
if (!$mysqli->select_db($bd))
{
  header("Location: data/crearData.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
     <script language="javascript" type="text/javascript">
      function saltar(pagina){
        document.form1.action=pagina;
        document.form1.submit();
      }
    </script>
  <title>My Library</title>
  <link rel="stylesheet" type="text/css" href="css/infoJuegos.css">

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/scrolling-nav.css" rel="stylesheet">

</head >

<body id="page-top" style="background-color:#299713">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav" style="background-image: url('imagenes/imagen1.jpg')">
    <div class="container" >
      <a class="navbar-brand js-scroll-trigger" href="#about">My Library</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <form id="form2" name="form2" method="post">
              <!--<label for="nomJuego"><b>Filtrar por plataforma</b> </label>-->
              <select name="nomPlat" id="nomPlat" style="background-color: #1166DB">
              <?php 
              require ("data/conecxion.php");
              $consulta ="SELECT nomPlat FROM plataforma;";
              $resultado=$mysqli->query($consulta);
              while ($fila = $resultado ->fetch_assoc())
                {
                  echo ("<option>".$fila["nomPlat"]."</option>");
                } 
              ?>
              </select>
             <input  id="button2" type="submit" value="Filtrar"/>
            </form>
            <?php @$nomPlat=$_POST["nomPlat"]; ?>
          </li>
          <li>
            &nbsp &nbsp
            <input id="button2" type="submit" value="Salir" onClick='javascript:saltar("index.php")'/>
          </li>
        </ul>
        </ul>
      </div>
    </div>
  </nav>

  <header class="bg-primary text-white" style="background-image: url('imagenes/imagen2.jpg')" >
    <div class="container text-center">
      <h1>&nbsp &nbsp</h1>
      <p class="lead"></p>
    </div>
  </header>
  <section id="about" >
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <!--MOSTRAR TABLAS REGISTROS-->
          <form id="form1" name="form1" method="POST" action="" >
<!--MOSTRAR PLataforma-->
<?php
require ("data/conecxion.php");
$consulta="SELECT * FROM plataforma;";
$resultado = $mysqli->query($consulta);
if (!$resultado) 
  {
   echo "Lo sentimos. La Aplicación no funciona<br>";
   echo "Error. en la consulta: ".$consulta."<br>";
   echo "Num.error: ".$mysqli->errno."<br>";
   echo "Error: ".$mysqli->error. "<br>";
   exit;
  }
?>
    <table border="0" cellpadding="10">
    <tr><th colspan="6">Plataforma</th></tr>
    <tr><th>Nombre</th>
    <th>Dispositivos</th>
    <th>NumJuegos</th>
    <th></th></tr>
     <?php
    while ($fila = $resultado->fetch_assoc()) {
      echo ("<tr><td>".$fila["nomPlat"]."</td>");
      echo ("<td>".$fila["dispositivo"]."</td>");
      echo ("<td>".$fila["numJuegos"]."</td>");
      echo ("<td><input type='radio' id='seleccionPlat' name='seleccionPlat' value='".$fila["nomPlat"]."'>"."</td>");
      echo ("</td>");
    }
  ?>
  </table>
<br><br><br><br>

<!--MOSTRAR JUEGOS--> 
<?php
//Si no esta definifa nomPlat  o está vacia lo muestra todo (La primera vez que entra lo mostrará todo)
if (!isset($nomPlat) || $nomPlat == "")
{
  $consulta="SELECT * FROM videojuego;";
  $resultado = $mysqli->query($consulta);
  if (!$resultado) 
    {echo "Lo sentimos. La Aplicación no funciona<br>";
     echo "Error. en la consulta: ".$consulta."<br>";
     echo "Num.error: ".$mysqli->errno."<br>";
     echo "Error: ".$mysqli->error. "<br>";
     exit;
    }
  ?>
      <table border="0" cellpadding="10">
      <tr><th colspan="6">VideoJuegos</th></tr>
      <tr><th>Nombre</th>
      <th>Plataforma</th>
      <th>Genero</th>
      <th>NumHoras</th>
      <th>Valoracion</th>
      <th>Opinion</th> 
      <th></th></tr> 
      <?php
      while ($fila = $resultado->fetch_assoc()) {
        echo ("<tr><td>".$fila["nomJuego"]."</td>");
        echo ("<td>".$fila["nomPlat"]."</td>");
        echo ("<td>".$fila["genero"]."</td>");
        echo ("<td>".$fila["numHoras"]."</td>");
        echo ("<td>".$fila["valoracion"]."</td>");
        echo ("<td>".$fila["opinion"]."</td>");
        echo ("<td><input type='radio' id='seleccionJuego' name='seleccionJuego' value='".$fila["id"]."'>"."</td>");
        echo ("</td>");
      }
      ?>
     </table>
    <?php
}else
{
  //Busqueda por el campo NomPlat
 $consulta="SELECT * FROM videojuego WHERE nomPlat='$nomPlat';";
  $resultado = $mysqli->query($consulta);
  if (!$resultado) 
    {echo "Lo sentimos. La Aplicación no funciona<br>";
     echo "Error. en la consulta: ".$consulta."<br>";
     echo "Num.error: ".$mysqli->errno."<br>";
     echo "Error: ".$mysqli->error. "<br>";
     exit;
    }
  ?>
      <table border="0" cellpadding="10">
      <tr><th colspan="6">VideoJuegos</th></tr>
      <tr><th>Nombre</th>
      <th>Plataforma</th>
      <th>Genero</th>
      <th>NumHoras</th>
      <th>Valoracion</th>
      <th>Opinion</th>  
      <th></th></tr>
      <?php
      while ($fila = $resultado->fetch_assoc()) {
        echo ("<tr><td>".$fila["nomJuego"]."</td>");
        echo ("<td>".$fila["nomPlat"]."</td>");
        echo ("<td>".$fila["genero"]."</td>");
        echo ("<td>".$fila["numHoras"]."</td>");
        echo ("<td>".$fila["valoracion"]."</td>");
        echo ("<td>".$fila["opinion"]."</td>");
        echo ("<td><input type='radio' id='seleccionJuego' name='seleccionJuego' value='".$fila["id"]."'>"."</td>");
        echo ("</td>");
      }
    ?>
     </table>
<?php
}
?>
<br><br>
<input id="button" type="submit" value="Modificar / Agregar Plataformas"onClick='javascript:saltar("formPlataforma.php")'/>
<input id="button" type="submit" value="Modificar / Agregar Juegos"onClick='javascript:saltar("formJuegos.php")'/>
<input id="button" type="submit" value="Eliminar" onClick='javascript:saltar("procesamientoDatos/eliminar.php")'/>
<input id="button" type="button" value="Mostrar Todos" onclick='javascript:saltar("infoJuegos.php")'/>
</form>
        </div>
      </div>
    </div>
  </section>
<!--END MOSTRAR DATOS-->
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">My Library - Una pequeña libreria para tus juegos</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="js/scrolling-nav.js"></script>

</body>

</html>
